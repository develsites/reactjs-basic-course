import React, {useState} from 'react';

const Counter = () => {
    const [count, setCouunt] = useState(0)

    function increment() {
        setCouunt(count + 1);
    }

    function decrement () {
        setCouunt(count - 1);
    }

    return (
        <div>
            <h1>{count}</h1>
            <button onClick={increment}>Increment</button>
            <button onClick={decrement}>Decrement</button>
        </div>
    );
};

export default Counter;
import {useState} from "react";

export const useFetching = (callback) => {
    const [isLoading, seIsLoading] = useState(false)
    const [error, setError] = useState('')

    const fetching = async (...args) => {
        try {
            seIsLoading(true)
            await callback(...args)
        } catch (e) {
            setError(e.message)
        } finally {
            seIsLoading(false)
        }
    }

    return [fetching, isLoading, error]
}